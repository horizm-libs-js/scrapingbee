const Tiktok = require('./libs/scrappingbee-tiktok');
const TiktokApi = require('./libs/tiktok-api');
const Instagram = require('./libs/scrappingbee-instagram');


module.exports = {
    Tiktok: Tiktok,
    TiktokApi: TiktokApi,
    Instagram: Instagram
}
