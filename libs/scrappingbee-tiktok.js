var rp = require('request-promise');
const jsdom = require('jsdom');

class Tiktok {

    constructor(api_key) {
        this.api_key = api_key
        this.uri = 'https://app.scrapingbee.com/api/v1?'
        this.try = 0;
        this.maxTry = 4;
    }

    async getUrlVideo(url) {
        const options = {
            uri: this.uri,
            qs: {
                'api_key': this.api_key,
                'url': url,
                'premium_proxy':'True',
            },

        };

        return await rp(options).then(response => {
            const dom = new jsdom.JSDOM(response)
            const data = dom.window.document.querySelector('#SIGI_STATE').textContent;
            let jsondata = JSON.parse(data);
            return jsondata.ItemList.video.preloadList;
        }).catch(async (err) => {
            this.try++;
            console.log("try again tiktok " + this.try);
            if (this.try <= this.maxTry) {
                return await this.getUrlVideo(url);
            } else {
                return false;
            }
        })
    }


}


module.exports = Tiktok;