var rp = require('request-promise');

class TiktokApi {

    constructor(api_key) {
        this.RapidAPIkey = api_key
        this.RapidAPIhost = 'tiktok-video-no-watermark2.p.rapidapi.com'
        this.try = 0;
        this.maxTry = 4;
    }

    async getUrlVideo(url) {

        const options = {
            method: 'GET',
            uri: 'https://'+ this.RapidAPIhost +'/',
            qs: {
                url: url,
                hd: '0'
            },
            headers: {
                'X-RapidAPI-Key': this.RapidAPIkey,
                'X-RapidAPI-Host': this.RapidAPIhost
            },
            json: true
        };

        return await rp(options).then(response => {
            return response;
        }).catch(async (err) => {
            this.try++;
            console.log("try again tiktok " + this.try);
            if (this.try <= this.maxTry) {
                return await this.getUrlVideo(url);
            } else {
                return false;
            }
        })
    }


}


module.exports = TiktokApi;