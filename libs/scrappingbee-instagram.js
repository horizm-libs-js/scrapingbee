var rp = require('request-promise');
const jsdom = require('jsdom')
const { JSDOM } = jsdom;


class Instagram {

    constructor(api_key){
        this.api_key=api_key
        this.uri='https://app.scrapingbee.com/api/v1?'
        this.try = 0;
        this.maxTry = 4;
    }

    async getUrlData(url){
        const options = {
            uri: this.uri,
            qs: {
                'api_key': this.api_key,
                'url': url,
                'block_resources':'False',
                'premium_proxy':'True',
                'js_scenario': '{"instructions":[{"evaluate":"console.log(window._sharedData)"}]}'
            }
        };



        return await rp(options).then(response => {
            const dom = new JSDOM(response, { runScripts: "dangerously"  });
            const data=dom.window


            const regex = /^https?\:\/\/([^\/?#]+)(?:|$)/gm;

            // Alternative syntax using RegExp constructor
            // const regex = new RegExp('https:\\/\\/scontent-[a-zA-Z][a-zA-Z][a-zA-Z][0-9]+-[0-9]+\\.cdninstagram\\.com', 'gm')

            const subst = `https://scontent.cdninstagram.com`;

            return {
                display_url: data.window._globalObject._sharedData.entry_data.PostPage[0].graphql.shortcode_media.display_url.replace(regex, subst),
                video_url: data.window._globalObject._sharedData.entry_data.PostPage[0].graphql.shortcode_media.video_url.replace(regex, subst)
            };


        }).catch(async (err) => {
            this.try++;
            console.log("try again instagram " + this.try);
            if (this.try <= this.maxTry) {
                return await this.getUrlVideo(url);
            } else {
                return false;
            }
        })
    }

    async getUrlVideo(url){
        const data= await this.getUrlData(url);
        return data.video_url;
    }


}


module.exports = Instagram;