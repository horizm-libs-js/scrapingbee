#!/usr/bin/env node
const horizmjs = require('../index');

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv


switch (argv.social) {
    case 'tiktok':
        client = new horizmjs.Tiktok(process.env.SCRAPPINGBEETOKEN);
        break;
    case 'instagram':
        client = new horizmjs.Instagram(process.env.SCRAPPINGBEETOKEN);
        break;
    case 'tiktokapi':
        client = new horizmjs.TiktokApi(process.env.RAPIDAPITIKTOK);
        break;

    default:
        console.log("not implement")
        break;
}

switch (argv.get) {
    case 'videourl':
        
        (async() => {
            console.log(await client.getUrlVideo(argv.url))
        })();

        break;

    default:
        console.log("not implement")
        break;
}

